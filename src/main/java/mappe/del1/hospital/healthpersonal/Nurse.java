package mappe.del1.hospital.healthpersonal;

import mappe.del1.hospital.Employee;

public class Nurse extends Employee {

    /**
     * Constructor method that creates new instance of a nurse through the Empoloyee class
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
