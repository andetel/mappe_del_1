package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Patient;

public class GeneralPractitioner extends Doctor {

    /**
     * Constructor method that creates a new instance of a general practitioner through the abstract Doctor class
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Method that sets a diagnosis on the provided patient
     * @param patient patient object
     * @param diagnosis string
     */
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
