package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Patient;

public class Surgeon extends Doctor {

    /**
     * Constructor method that creates a new instance of a surgeon through the abstract Doctor class
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Method that sets a diagnosis on the specified patient
     * @param patient patient object
     * @param diagnosis string
     */
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
