package mappe.del1.hospital.exception;

public class RemoveException extends Exception{
    private static final long serialVersionUID = 1L;

    /**
     * Thrown if the remove method in Department is not able to remove the person
     * @param message custom error message
     */
    public RemoveException(String message) {
        super(message);
    }
}
