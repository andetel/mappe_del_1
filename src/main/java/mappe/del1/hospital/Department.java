package mappe.del1.hospital;

import com.sun.jdi.request.DuplicateRequestException;
import mappe.del1.hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.Objects;

public class Department {
    private String departmentName;
    private ArrayList<Employee> employees;
    private ArrayList<Patient> patients;

    /**
     * Constructor method that create a new instance of a department
     * @param name
     */
    public Department(String name) {
        this.departmentName = name;
        this.employees = new ArrayList<Employee>();
        this.patients = new ArrayList<Patient>();
    }

    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Method that gets all employees from the department
     * @return ArrayList
     */
    public ArrayList<Employee> getEmployees() {
        ArrayList<Employee> cpEmployees = new ArrayList<Employee>();

        for (Employee employee: this.employees) {
            cpEmployees.add(employee);
        }

        return cpEmployees;

    }

    /**
     * Method that gets all patients from the department
     * @return ArrayList
     */
    public ArrayList<Patient> getPatients() {
        ArrayList<Patient> cpPatients = new ArrayList<Patient>();

        for (Patient patient: this.patients) {
            cpPatients.add(patient);
        }

        return cpPatients;
    }

    public void setDepartmentName(String name) {
        this.departmentName = name;
    }

    /**
     * Method that adds the specified employee to the department
     * @param employee employee object
     */
    public void addEmployee(Employee employee) {

        for(Employee em: this.employees) {
            if(em.toString().equals(employee.toString())) {
                throw new IllegalArgumentException("The employee is already in the list.");
            }
        }

        this.employees.add(new Employee(employee.getFirstName(), employee.getLastName(), employee.getSocialSecurityNumber()));
    }

    /**
     * Method that adds the specified patient to the department
     * @param patient patient object
     */
    public void addPatient(Patient patient) {

        for(Patient pa: this.patients) {
            if(pa.toString().equals(patient.toString())) {
                throw new IllegalArgumentException("The patient is already in the list.");
            }
        }

        this.patients.add(new Patient(patient.getFirstName(), patient.getLastName(), patient.getSocialSecurityNumber()));
    }

    /**
     * Method that removes either a patient or an employee from the department.
     * @param person person object
     */
    public void remove(Person person) throws RemoveException {

        if (!(person instanceof Employee) && !(person instanceof Patient)) {
            throw new RemoveException("The provided person is not an employee nor a patient.");
        }

        int inList = 0;

        if (person instanceof Employee) {
            Employee em = (Employee) person;
            for (int i = 0; i < this.employees.size(); i++) {
                if (this.employees.get(i).toString().equals(em.toString())) {
                    this.employees.remove(i);
                    inList = 1;
                    break;
                }
            }
        } else {
            Patient pa = (Patient) person;
            for (int j = 0; j < this.patients.size(); j++) {
                if (this.patients.get(j).toString().equals(pa.toString())) {
                    this.patients.remove(j);
                    inList = 1;
                    break;
                }
            }
        }

        if (inList == 0) {
            throw new RemoveException("The specified person was not found in either lists.");
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || this.getClass() != o.getClass()) return false;

        Department that = (Department) o;
        return Objects.equals(this.departmentName, that.departmentName) &&
                Objects.equals(this.employees, that.employees) &&
                Objects.equals(this.patients, that.patients);
    }

    /**
     *
     * @return departmentname
     */
    @Override
    public String toString() {
        return "Departmentname: " + this.getDepartmentName();
    }
}
