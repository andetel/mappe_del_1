package mappe.del1.hospital;

/**
 * Interface that specifies whether a person can set a diagnosis.
 * Has the method setDiagnosis.
 */
public interface Diagnosable {
    public void setDiagnosis(String diagnose);
}
