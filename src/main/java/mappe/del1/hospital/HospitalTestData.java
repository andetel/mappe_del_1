package mappe.del1.hospital;

import mappe.del1.hospital.healthpersonal.doctor.GeneralPractitioner;
import mappe.del1.hospital.healthpersonal.doctor.Surgeon;
import mappe.del1.hospital.healthpersonal.Nurse;


public final class HospitalTestData {

    private HospitalTestData() {
        // Not called
    }

    /**
     * @param hospital
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {

        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.addEmployee(new Employee("Odd Even", "Primtallet", ""));
        emergency.addEmployee(new Employee("Huppasahn", "DelFinito", ""));
        emergency.addEmployee(new Employee("Rigmor", "Mortis", ""));
        emergency.addEmployee(new GeneralPractitioner("Inco", "Gnito", ""));
        emergency.addEmployee(new Surgeon("Inco", "Gnito", ""));
        emergency.addEmployee(new Nurse("Nina", "Teknologi", ""));
        emergency.addEmployee(new Nurse("Ove", "Ralt", ""));
        emergency.addPatient(new Patient("Inga", "Lykke", ""));
        emergency.addPatient(new Patient("Ulrik", "Smål", ""));

        hospital.addDepartment(emergency);

        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.addEmployee(new Employee("Salti", "Kaffen", ""));
        childrenPolyclinic.addEmployee(new Employee("Nidel V.", "Elvefølger", ""));
        childrenPolyclinic.addEmployee(new Employee("Anton", "Nym", ""));
        childrenPolyclinic.addEmployee(new GeneralPractitioner("Gene", "Sis", ""));
        childrenPolyclinic.addEmployee(new Surgeon("Nanna", "Na", ""));
        childrenPolyclinic.addEmployee(new Nurse("Nora", "Toriet", ""));
        childrenPolyclinic.addPatient(new Patient("Hans", "Omvar", ""));
        childrenPolyclinic.addPatient(new Patient("Laila", "La", ""));
        childrenPolyclinic.addPatient(new Patient("Jøran", "Drebli", ""));

        hospital.addDepartment(childrenPolyclinic);

    }
}
