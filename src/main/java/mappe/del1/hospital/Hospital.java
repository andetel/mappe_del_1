package mappe.del1.hospital;

import java.util.ArrayList;

public class Hospital {
    private final String hospitalName;
    private ArrayList<Department> departments;

    /**
     * Constructor method that creates a new instance of a hospital
     * @param name
     */
    public Hospital(String name) {
        this.hospitalName = name;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Method to get all departments from the hospital
     * @return ArrayList
     */
    public ArrayList<Department> getDepartments() {
        ArrayList<Department> cpDepartments = new ArrayList<Department>();

        for (Department department: this.departments) {
            cpDepartments.add(department);
        }

        return cpDepartments;
    }

    /**
     * Method that adds the specified department to the hospital
     * @param department department object
     */
    public void addDepartment(Department department) {
        this.departments.add(department);
    }

    /**
     *
     * @return name of the hospital
     */
    @Override
    public String toString() {
        return "Hospitalname: " + this.getHospitalName();
    }
}
