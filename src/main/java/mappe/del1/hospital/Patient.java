package mappe.del1.hospital;

public class Patient extends Person implements Diagnosable{
    private String diagnosis = "";

    /**
     * Constructor method that creates a new instance of a patient through the Person class
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    protected String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
