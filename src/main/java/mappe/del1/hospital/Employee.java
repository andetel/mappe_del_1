package mappe.del1.hospital;

public class Employee extends Person{

    /**
     * Constructor method that creates a new instance of an employee through the Person class
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
