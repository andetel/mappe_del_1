package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;

public class HospitalClient {
    public static void main(String[] args) throws RemoveException {
        Hospital stOlav = new Hospital("St. Olav");

        HospitalTestData.fillRegisterWithTestData(stOlav);


        // Code below removes employee nr. 7 from department "Akutten"
        // stOlav.getDepartments().get(0).remove(stOlav.getDepartments().get(0).getEmployees().get(6));

        // Try-catch block to try to remove patient that isn't inside the provided list.
        // The finally block removes a patient that actually exists in the list of patients
        // in one of the departments.
        try {
            stOlav.getDepartments().get(0).remove(stOlav.getDepartments().get(1).getPatients().get(2));
        } catch (RemoveException e) {
            System.out.println(e);
        } finally {
            stOlav.getDepartments().get(0).remove(stOlav.getDepartments().get(0).getPatients().get(1));
        }


        // Below code prints out the name of every department, along with all the employees and patients
        // for each of the departments.
        for (Department department: stOlav.getDepartments()) {
            System.out.println(department.toString() + "\n");
            System.out.println("Employees: \n");

            for (Employee employee: department.getEmployees()) {
                System.out.println(employee.toString());
            }

            System.out.println("\nPatients: \n");
            for (Patient patient: department.getPatients()) {
                System.out.println(patient.toString());
            }
            System.out.println("\n");
        }
    }
}
