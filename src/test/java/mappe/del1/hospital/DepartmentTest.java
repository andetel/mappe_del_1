package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;
import mappe.del1.hospital.healthpersonal.*;
import mappe.del1.hospital.healthpersonal.doctor.GeneralPractitioner;
import mappe.del1.hospital.healthpersonal.doctor.Surgeon;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;

class DepartmentTest {

    @Nested
    class removePersonThatIsInTheRegistry {

        @Test
        void removeEmployeeInRegistry() throws RemoveException {
            Department department = new Department("Emergency");

            Surgeon per = new Surgeon("Per", "Persen", "101010");
            Nurse kari = new Nurse("Kari", "Katt", "102030");
            GeneralPractitioner ola = new GeneralPractitioner("Ola", "Nordmann", "104060");
            Nurse rigmor = new Nurse("Rigmor", "Rigmorsen", "105070");

            Patient odd = new Patient("Odd", "Even", "202020");
            Patient inco = new Patient("Inco", "Gnito", "203040");
            Patient ove = new Patient("Ove", "Ralt", "205070");

            department.addEmployee(per);
            department.addEmployee(kari);
            department.addEmployee(ola);
            department.addEmployee(rigmor);

            department.addPatient(odd);
            department.addPatient(inco);
            department.addPatient(ove);

            department.remove(per);

            Employee em = null;

            for(Employee employee: department.getEmployees()) {
                if(employee.toString().equals(per.toString())) {
                    em = employee;
                }
            }

            Assertions.assertNull(em);

        }

        @Test
        void removePatientInRegistry() throws RemoveException {
            Department department = new Department("Emergency");

            Surgeon per = new Surgeon("Per", "Persen", "101010");
            Nurse kari = new Nurse("Kari", "Katt", "102030");
            GeneralPractitioner ola = new GeneralPractitioner("Ola", "Nordmann", "104060");
            Nurse rigmor = new Nurse("Rigmor", "Rigmorsen", "105070");

            Patient odd = new Patient("Odd", "Even", "202020");
            Patient inco = new Patient("Inco", "Gnito", "203040");
            Patient ove = new Patient("Ove", "Ralt", "205070");

            department.addEmployee(per);
            department.addEmployee(kari);
            department.addEmployee(ola);
            department.addEmployee(rigmor);

            department.addPatient(odd);
            department.addPatient(inco);
            department.addPatient(ove);

            department.remove(odd);

            Patient pa = null;

            for(Patient patient: department.getPatients()) {
                if(patient.toString().equals(odd.toString())) {
                    pa = patient;
                }
            }

            Assertions.assertNull(pa);

        }

    }

    @Nested
    class removePersonThatIsNotInTheRegistry {

        @Test
        void removeEmployeeNotInRegistry() throws RemoveException {
            Department department = new Department("Emergency");

            Surgeon per = new Surgeon("Per", "Persen", "101010");
            Nurse kari = new Nurse("Kari", "Katt", "102030");
            GeneralPractitioner ola = new GeneralPractitioner("Ola", "Nordmann", "104060");
            Nurse rigmor = new Nurse("Rigmor", "Rigmorsen", "105070");

            Patient odd = new Patient("Odd", "Even", "202020");
            Patient inco = new Patient("Inco", "Gnito", "203040");
            Patient ove = new Patient("Ove", "Ralt", "205070");

            department.addEmployee(per);
            department.addEmployee(kari);
            department.addEmployee(ola);

            department.addPatient(odd);
            department.addPatient(inco);
            department.addPatient(ove);

            Assertions.assertThrows(RemoveException.class, () ->
                    department.remove(rigmor)
            );
        }

        @Test
        void removePatientNotInRegistry() throws RemoveException {
            Department department = new Department("Emergency");

            Surgeon per = new Surgeon("Per", "Persen", "101010");
            Nurse kari = new Nurse("Kari", "Katt", "102030");
            GeneralPractitioner ola = new GeneralPractitioner("Ola", "Nordmann", "104060");

            Patient odd = new Patient("Odd", "Even", "202020");
            Patient inco = new Patient("Inco", "Gnito", "203040");
            Patient ove = new Patient("Ove", "Ralt", "205070");

            department.addEmployee(per);
            department.addEmployee(kari);
            department.addEmployee(ola);

            department.addPatient(odd);
            department.addPatient(ove);

            Assertions.assertThrows(RemoveException.class, () ->
                    department.remove(inco)
            );
        }
    }

    @Nested
    class removeAPersonWhenPatientAndEmployeeHasSameName {

        @Test
        void removePatientWithSameNameAsEmployee() throws RemoveException {
            Department department = new Department("Emergency");

            Surgeon per = new Surgeon("Per", "Persen", "101010");
            Nurse kari = new Nurse("Kari", "Katt", "102030");
            GeneralPractitioner ola = new GeneralPractitioner("Ola", "Nordmann", "104060");

            Patient per1 = new Patient("Per", "Persen", "101010");
            Patient inco = new Patient("Inco", "Gnito", "203040");
            Patient ove = new Patient("Ove", "Ralt", "205070");

            department.addEmployee(per);
            department.addEmployee(kari);
            department.addEmployee(ola);

            department.addPatient(per1);
            department.addPatient(inco);
            department.addPatient(ove);

            department.remove(per1);

            Patient pa = null;

            for(Patient patient: department.getPatients()) {
                if(patient.toString().equals(per1.toString())) {
                    pa = patient;
                }
            }

            Assertions.assertNull(pa);

        }

        @Test
        void removeEmployeeWithSameNameAsPatient() throws RemoveException{
            Department department = new Department("Emergency");

            Surgeon per = new Surgeon("Per", "Persen", "101010");
            Nurse kari = new Nurse("Kari", "Katt", "102030");
            GeneralPractitioner ola = new GeneralPractitioner("Ola", "Nordmann", "104060");

            Patient per1 = new Patient("Per", "Persen", "101010");
            Patient inco = new Patient("Inco", "Gnito", "203040");
            Patient ove = new Patient("Ove", "Ralt", "205070");

            department.addEmployee(per);
            department.addEmployee(kari);
            department.addEmployee(ola);

            department.addPatient(per1);
            department.addPatient(inco);
            department.addPatient(ove);

            department.remove(per);

            Employee em = null;

            for(Employee employee: department.getEmployees()) {
                if(employee.toString().equals(per.toString())) {
                    em = employee;
                }
            }

            Assertions.assertNull(em);
        }
    }

    @Nested
    class testsToTryToAddDuplicateObjectsToLists {

        @Test
        void addAlreadyExistingEmployeeToList() throws IllegalArgumentException{
            Department department = new Department("Emergency");

            Surgeon per = new Surgeon("Per", "Persen", "101010");
            Nurse per1 = new Nurse("Per", "Persen", "101010");
            GeneralPractitioner ola = new GeneralPractitioner("Ola", "Nordmann", "104060");
            Nurse rigmor = new Nurse("Rigmor", "Rigmorsen", "105070");

            department.addEmployee(per);
            department.addEmployee(ola);
            department.addEmployee(rigmor);

            Assertions.assertThrows(IllegalArgumentException.class, () ->
                department.addEmployee(per1)
            );

        }

        @Test
        void addAlreadyExistingPatientToList() throws IllegalArgumentException{
            Department department = new Department("Emergency");

            Patient odd = new Patient("Odd", "Even", "202020");
            Patient odd1 = new Patient("Odd", "Even", "202020");
            Patient inco = new Patient("Inco", "Gnito", "203040");
            Patient ove = new Patient("Ove", "Ralt", "205070");

            department.addPatient(odd);
            department.addPatient(inco);
            department.addPatient(ove);

            Assertions.assertThrows(IllegalArgumentException.class, () ->
                    department.addPatient(odd1)
            );

        }
    }

}